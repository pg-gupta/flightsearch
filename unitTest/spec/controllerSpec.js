
describe('controller: mainController', function() {
  var ctrl, getDayService,processFlightsService, $scope,flights;

  beforeEach(module('searchFlightApp'));

  beforeEach(inject(function($rootScope, $controller) {
    
    getDayService = function(){
    	return 'Monday';
    }

    processFlightsService={
    	flights: [{
		"flightNo":"AI-206",
		"returnFlight":"AI-207",
		"source":"Pune",
		"srcCode":"PNQ",
		"destination":"Delhi",
		"destCode":"DLH",
		"price":500,
		"days":["Monday","Tuesday","Friday"],
		"departure":"3.02AM(IST)",
		"arrival":"7:15AM(IST)"		
	},
	{
		"flightNo":"AI-207",
		"returnFlight":"AI-206",
		"source":"Delhi",
		"srcCode":"DLH",
		"destination":"Pune",
		"destCode":"PNQ",
		"price":500,	
		"days":["Monday","Tuesday","Friday"],	
		"departure":"10.00PM",
		"arrival":"01.00AM"
	}]
    }

    $scope = $rootScope.$new();

    ctrl = $controller('mainController', {$scope: $scope ,getDayService: getDayService,processFlightsService:processFlightsService});
  }));

// Positive Test Cases
  it('Should search a flight for One Way journey', function() {
  	var flight=[{
		"flightNo":"AI-206",
		"returnFlight":"AI-207",
		"source":"Pune",
		"srcCode":"PNQ",
		"destination":"Delhi",
		"destCode":"DLH",
		"price":500,
		"days":["Monday","Tuesday","Friday"],
		"departure":"3.02AM(IST)",
		"arrival":"7:15AM(IST)"		
	}];

	$scope.isOneWay=true;
  	$scope.source="Pune",
  	$scope.destination="Delhi",
  	$scope.depDate="12-03-2016";

  	$scope.search();
    expect($scope.hasResults).toBe(true);
    expect($scope.result[0].flight.flightNo).toBe(flight[0].flightNo);
  });

  it('Should search a flight for a Return journey', function() {
  	var flight=[{
		"flightNo":"AI-206",
		"returnFlight":"AI-207",
		"source":"Pune",
		"srcCode":"PNQ",
		"destination":"Delhi",
		"destCode":"DLH",
		"price":500,
		"days":["Monday","Tuesday","Friday"],
		"departure":"3.02AM(IST)",
		"arrival":"7:15AM(IST)"		
	},
	{
		"flightNo":"AI-207",
		"returnFlight":"AI-206",
		"source":"Delhi",
		"srcCode":"DLH",
		"destination":"Pune",
		"destCode":"PNQ",
		"price":500,	
		"days":["Monday","Tuesday","Friday"],	
		"departure":"10.00PM",
		"arrival":"01.00AM"
	}];

	// Instantiate variables
	$scope.isOneWay=false;
  	$scope.source="Pune",
  	$scope.destination="Delhi",
  	$scope.depDate="12-03-2016";
  	$scope.retDate="18-03-2016";

  	// Call the search function
  	$scope.search();

    expect($scope.hasResults).toBe(true);
    expect($scope.result[0].flight.flightNo).toBe(flight[0].flightNo);
    expect($scope.result[0].returnFlight.flightNo).toBe(flight[1].flightNo);
  });

// Negative Test Cases
	
	it("Should not return flight for One Way Journey", function(){
		$scope.source="Chennai",
		$scope.destination="Banglore",
  		$scope.depDate="12-03-2016";

		$scope.search();

		expect($scope.hasResults).not.toBe(true);
		// below test will fail
		// expect($scope.hasResults).toBe(true);

	})

});
