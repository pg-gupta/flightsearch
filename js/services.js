
// get day of the week
searchFlightApp.factory('getDayService', function(){
	var week=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
	return function(input){
		return week[input];
	}
})

// service to read flights data from a file or json object
searchFlightApp.factory('processFlightsService',function(){

	var data=[
	{
		"flightNo":"KF-200",
		"returnFlight":"KF-201",
		"source":"Mumbai",
		"srcCode":"MBI",
		"destination":"Kolkatta",
		"destCode":"KTA",
		"price":1000,
		"days":["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],
		"departure":"3.02AM",
		"arrival":"7:15AM"		
	},
	{
		"flightNo":"KF-201",
		"returnFlight":"KF-200",
		"source":"Kolkatta",
		"srcCode":"KTA",
		"destination":"Mumbai",
		"destCode":"MBI",
		"price":1000,	
		"days":["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],	
		"departure":"10.00PM",
		"arrival":"01.00AM"
	},
	{
		"flightNo":"AI-200",
		"returnFlight":"AI-201",
		"source":"Mumbai",
		"srcCode":"MBI",
		"destination":"Delhi",
		"destCode":"DLH",
		"price":1000,
		"days":["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],	
		"departure":"3.02AM",
		"arrival":"7:15AM"		
	},
	{
		"flightNo":"AI-201",
		"returnFlight":"AI-200",
		"source":"Delhi",
		"srcCode":"DLH",
		"destination":"Mumbai",
		"destCode":"MBI",
		"price":1000,	
		"days":["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],	
		"departure":"10.00PM",
		"arrival":"01.00AM"
	},
	{
		"flightNo":"AI-206",
		"returnFlight":"AI-207",
		"source":"Pune",
		"srcCode":"PNQ",
		"destination":"Delhi",
		"destCode":"DLH",
		"price":500,
		"days":["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],	
		"departure":"3.02AM",
		"arrival":"7:15AM"		
	},
	{
		"flightNo":"AI-207",
		"returnFlight":"AI-206",
		"source":"Delhi",
		"srcCode":"DLH",
		"destination":"Pune",
		"destCode":"PNQ",
		"price":500,	
		"days":["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],	
		"departure":"10.00PM",
		"arrival":"01.00AM"
	},
	{
		"flightNo":"TK06",
		"source":"Pune",
		"srcCode":"PNQ",
		"destination":"Chennai",
		"destCode":"CHN",
		"price":500,
		"days":["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],	
		"departure":"3.02AM",
		"arrival":"7:15AM"
	},
	{
		"flightNo":"TK12",
		"source":"Banglore",
		"destination":"Chennai",
		"price":1000,
		"days":["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],	
		"departure":"3.02AM",
		"arrival":"7:15AM"
	},
	{
		"flightNo":"AI-006",
		"returnFlight":"AI-007",
		"source":"Pune",
		"srcCode":"PNQ",
		"destination":"Delhi",
		"destCode":"DLH",
		"price":1000,
		"days":["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],	
		"departure":"3.02AM(IST)",
		"arrival":"7:15AM(IST)"
		
	},
	{
		"flightNo":"AI-007",
		"returnFlight":"AI-006",
		"source":"Delhi",
		"srcCode":"DLH",
		"destination":"Pune",
		"destCode":"PNQ",
		"price":500,	
		"days":["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],	
		"departure":"10.00PM",
		"arrival":"01.00AM"
	},
	];

	return{flights:data}

})


