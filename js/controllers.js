
searchFlightApp.controller('mainController',['$scope','getDayService','processFlightsService','rateFilter',function($scope,getDayService,processFlightsService,rateFilter){
    
    // initialization
	$scope.rate=10000;
	$scope.isOneWay=true;
	$scope.isHome=true;
	$scope.message="Your Results";

	// get the flights data through processFlightService
	var data=processFlightsService.flights;
	$scope.data=processFlightsService.flights;

	// switch between OneWay and Return
	$scope.switchTravelType=function(){
		$scope.isOneWay=!$scope.isOneWay;
		$scope.hasResults=false;
		$(event.target).parent().siblings().removeClass('active');		
		$(event.target).parent().addClass('active');
	};

	// to search for a flight
	$scope.search=function(){		
		
		$scope.result=[];
		$scope.hasResults=false;
		var depDate=new Date($scope.depDate);
		var depDay= getDayService(depDate.getDay());
		
		// get day of return date
		var retDate=new Date($scope.retDate);
		var retDay= getDayService(retDate.getDay());

		if ($scope.depDate==undefined && $scope.isOneWay) {$scope.message="Please enter departure date"}
			else if ($scope.retDate==undefined && !$scope.isOneWay){$scope.message="Please enter return date"}									
				else if(depDate>retDate){$scope.message="Return Date Should be greater than Departure Date";}
					else
					{
						angular.forEach(data,function(flight){		
							if (flight.source.toLowerCase()==$scope.source.toLowerCase() 
								&& flight.destination.toLowerCase()==$scope.destination.toLowerCase()
							 	&& $.inArray(depDay, flight.days)>-1) 
							{
								$scope.src=$scope.source.toUpperCase();
								$scope.dest=$scope.destination.toUpperCase();
								$scope.returnFlight=null;
								
								// if journey type is Return
								if (!$scope.isOneWay) {		
																								
										// get return flight
										$scope.getReturnFlight(flight.returnFlight);

									if ($scope.returnFlight && $.inArray(retDay,$scope.returnFlight.days)>-1) {					
										$scope.hasResults=true;				
										$scope.result.push({
											"flight":flight,
											"returnFlight":$scope.returnFlight
										});					
									}
												
							}
							else // if journey type is one way
							{
								$scope.hasResults=true;								
								$scope.result.push({
									"flight":flight
								})
							}
							}
							else
								$scope.message="No Results Found!"
						})
					}
	};

	// to get return flight for a flight
	$scope.getReturnFlight=function(flightNo){
		angular.forEach(data,function(flight){
			if (flight.flightNo==flightNo) {$scope.returnFlight= flight;}
		})
	}

	// to switch between Home and Search tab
	$scope.switchPage=function(argument) {		
		if (argument==1) {
			$scope.isHome=true;
		}
		else
			$scope.isHome=false;
		
		$(event.target).parent().siblings().removeClass('active');
		$(event.target).parent().addClass('active');
	}

}]);