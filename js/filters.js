
// custom filter to refine search by rate (customized to work for return type as well where the rate is doubled due to return journey)
searchFlightApp.filter('rate',function() {
	return function (input,scope) {
	var out=[];
		angular.forEach(input,function (item) {
			if (scope.isOneWay) {	
				if (item.flight.price<=scope.rate) 
				out.push(item)
			}
			else
			{
				if (item.flight.price*2<=scope.rate) {
					out.push(item);
				}
			}
		})
		return out;		
	}
})